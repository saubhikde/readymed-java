package readymed.sk.webservice;



import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;


import readymed.sk.dao.OrderDao;
import readymed.sk.dao.PartialAssignFetchDao;
import readymed.sk.dao.PrescriptionViewDao;
import readymed.sk.dto.PartialAssignFetchBean;
import readymed.sk.dto.Product;
import readymed.sk.model.UserDetailsFetchesModel;
import readymed.sk.transformer.FetchTransform;



import com.sun.jersey.multipart.FormDataParam;

@Path("/partialfetch")
public class PartialAssignFetch {
	@POST
	// Path: http://localhost/<appln-folder-name>/login/dologin
	@Path("/dopartialfetch")
	@Consumes(MediaType.APPLICATION_JSON)
	// Produces JSON as response
	@Produces(MediaType.APPLICATION_JSON) 
	// Query parameters are parameters: http://localhost/<appln-folder-name>/login/dologin?username=abc&password=xyz
	public JSONObject doLogin(PartialAssignFetchBean partialAssignFetchBean) throws Exception{
		JSONObject obj1  = new JSONObject();
		System.out.println("This is here");
		try 
		{
			String assignid = partialAssignFetchBean.getAid();
			System.out.println(assignid);
			
			JSONArray jArray1 = new JSONArray();
			jArray1 = PartialAssignFetchDao.GetFeeds(assignid);
			
			//
			
			/////////////////////////////////////////////////////
			
			Double cart_amount = 0.00;
			Double discount = 0.00;
			
			for(int i=0;i<jArray1.length(); i++){
				JSONObject obj = (JSONObject) jArray1.get(i);
				
				
				cart_amount = cart_amount + (Double)obj.get("product_amount");
				discount  = discount + (Double)obj.get("product_discount");
				
				
			}
			
			
			
				JSONObject obj = (JSONObject) jArray1.get(0);
				
				
				int oid = Integer.parseInt(obj.get("order_id").toString());
				
				
				obj1 = PartialAssignFetchDao.GetFeedsCoupon(oid);
				
				obj1.put("net_cart_amount", cart_amount);
				obj1.put("cart_discount_amount", discount);
				
				
				obj1.put("products",jArray1);
				
				 PrescriptionViewDao prescriptionViewDao = new PrescriptionViewDao();
				 JSONArray jArray2 = prescriptionViewDao.GetPrescription(oid);
				 
				 obj1.put("prescription", jArray2);
				
				
			
			
			
			/////////////////////////////////////////////////////

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("error");
		}
		return obj1;		
	}
}
