package readymed.sk.webservice;





import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import readymed.sk.dao.PrescriptionUploadDao;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
 
/**
 * @author Saurabh Kar
 *
 */
@Path("/upload")
public class UploadPrescription {
 
	@POST
	@Path("/doupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
		@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail,
		@FormDataParam("orderid") String oid) throws SQLException, Exception {
 
		
		    	String uploadedFileLocation = "C://Program Files/Apache Software Foundation/Tomcat 7.0/webapps/MedicineStrore/images/" + fileDetail.getFileName();
 
		    	
		    	//C://Program Files/Apache Software Foundation/Tomcat 7.0/webapps/MedicineStrore/images/
		    	
		    	System.out.println(oid);
		// save it
		
		
		    	String imageurl = "http://ec2-52-11-249-135.us-west-2.compute.amazonaws.com:8080/MedicineStrore/images/"+fileDetail.getFileName();
		    	
		
		    	writeToFile(uploadedInputStream, uploadedFileLocation);
		
		    	String output = "File uploaded to : " + uploadedFileLocation;
 
		    	try{
		       boolean status = PrescriptionUploadDao.insertImageUrl(oid, uploadedFileLocation,imageurl);
		    	}catch (Exception e) {
					e.printStackTrace();
				}
	          	return Response.status(200).entity(output).build();
		
 
	}
 
	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
		String uploadedFileLocation) {
 
		try {
			OutputStream out = new FileOutputStream(new File(
					uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];
 
			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
 
			e.printStackTrace();
		}
 
	}
	
	/////////////////////////////////////////////////////////////////////////
	
	@POST
	@Path("/uploadS3")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFileS3(
		@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail) {
 
		/*String uploadedFileLocation = "d://folderA/" + fileDetail.getFileName();
 
		// save it
		writeToFile(uploadedInputStream, uploadedFileLocation);
 
		String output = "File uploaded to : " + uploadedFileLocation;
 
		return Response.status(200).entity(output).build();*/
		final String SUFFIX = "/"; 
		final String bucketName = "frankrossbucket";
		 final String folderName = "imageofprescription";
		
		AWSCredentials credentials = new BasicAWSCredentials(
				"YourAccessKeyID",
				"YourSecretAccessKey");
		AmazonS3 s3client = new AmazonS3Client(credentials);
		
		String fileName = folderName+ SUFFIX + fileDetail.getName();
		
		s3client.putObject(new PutObjectRequest(bucketName, fileName,
				new File(fileDetail.getFileName()))
				.withCannedAcl(CannedAccessControlList.PublicRead));
		
		
		String output = "File uploaded " ;
		return Response.status(200).entity(output).build();
	}
	
	//////////////////////////////////////////////////////////////////////////
	
	
}