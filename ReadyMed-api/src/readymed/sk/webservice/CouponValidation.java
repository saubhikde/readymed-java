package readymed.sk.webservice;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import readymed.sk.dao.CouponFetchDao;
import readymed.sk.dao.UserUpdateDao;
import readymed.sk.dao.ValidateCouponDao;
import readymed.sk.dto.CouponBean;
import readymed.sk.dto.UpdateUserBean;
import readymed.sk.utility.Utitlity;




/**
 * @SaurabhKar
 *
 */
@Path("/coupValidate")
public class CouponValidation {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/doCoupValidate")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doValidate(CouponBean couponBean) throws SQLException, Exception{
			String response = "";
			String id = couponBean.getCode();
			
			
			
			int retCode = chckCoupon(id);
			if(retCode == 0){
				
				JSONArray jarray = CouponFetchDao.GetFeedsAllbycouCode(id);
				
				response = jarray.toString();
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
				
				
					String tresponse = Utitlity.constructJSON(" Coupon Not Exist",false);
			
					JSONObject jsonObject = new JSONObject(tresponse);
					
					JSONArray jarray = new JSONArray();
					jarray.put(jsonObject);
					
					 response = jarray.toString();
					
			}
			return response;
					
		}
		
		private int chckCoupon(String id){
			
			int result = 3;
			if(Utitlity.isNotNull(id)){
				try {
					if(ValidateCouponDao.validate(id)){
						System.out.println("Coupon Exist");
						result = 0;
					}
				} 
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
	
}

