package readymed.sk.webservice;



import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;





import com.sun.jersey.multipart.FormDataParam;

import readymed.sk.dao.AssignInsertDao;
import readymed.sk.dao.OrderDao;
import readymed.sk.dao.RegisterDao;
import readymed.sk.dao.RejectOrderDao;
import readymed.sk.dao.ShopLatLongDao;
import readymed.sk.dao.UserLatLongDao;
import readymed.sk.dto.LatLongBean;
import readymed.sk.dto.Product;
import readymed.sk.dto.RegisterBean;
import readymed.sk.dto.UserLatLongBean;
import readymed.sk.utility.Distancecalcualation;
import readymed.sk.utility.GCMAppStandAlone;
import readymed.sk.utility.SortHasMap;
import readymed.sk.utility.Utitlity;





/**
 * @SaurabhKar
 *
 */
@Path("/order")
public class Order {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/doorder")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doLogin(JSONObject inputJsonObj) throws SQLException, Exception{
			String response = "";
			ArrayList<Product> productLists = new ArrayList<Product>();
			
			
			String userid = (String) inputJsonObj.get("userid");
			
			System.out.println(userid);
			
			String datetime = (String) inputJsonObj.get("date_time");
			
			System.out.println(datetime);
			
			String cartamount = (String) inputJsonObj.get("cart_amount");
			
			System.out.println(cartamount);
			
			String cartdiscount = (String) inputJsonObj.get("cart_discount");
			
			System.out.println(cartdiscount);
			
			String netcartamount = (String) inputJsonObj.get("net_cart_amount");
			
			System.out.println(netcartamount);
			
			
			String cid = (String) inputJsonObj.get("coupon_id");
			
			String c_dis = (String) inputJsonObj.get("coupon_discount");
			
			String t_net_amt = (String) inputJsonObj.get("total_net_amount");
			
			JSONArray inner_arr = (JSONArray) inputJsonObj.get("product");
			
			for(int i=0;i<inner_arr.length(); i++){
				JSONObject obj = (JSONObject) inner_arr.get(i);
				System.out.println(obj.get("product_id"));
				
				Product productDto = new Product();
				productDto.setUser_id(userid);
				productDto.setDate_time(datetime);
				productDto.setCartamount(cartamount);
				productDto.setCartDiscount(cartdiscount);
				productDto.setNetcartamount(netcartamount);
				productDto.setCoupon_id(cid);
				productDto.setCoupon_discount(c_dis);
				productDto.setTot_net_amount(t_net_amt);
				productDto.setProd_id(obj.get("product_id").toString());
				productDto.setAmount(obj.get("amount").toString());
				productDto.setQuantity(obj.get("quantity").toString());
				productDto.setDis_amount(obj.get("dis_amount").toString());
				productDto.setNet_amount(obj.get("net_amount").toString());
				
				productLists.add(productDto);
				
			}
			
			int retCode = userOrder(productLists);
			if(retCode == 0){
				
				int orderId = OrderDao.mgetOrderId(productLists);
				
				String orderidStr = String.valueOf(orderId);
				////////////////////////////////////////////////////////
				
				UserLatLongDao userLatLongDao = new UserLatLongDao();
				ShopLatLongDao shopLatLongDao = new ShopLatLongDao();
				
				ArrayList<UserLatLongBean> u_lat_lon_list_beans =userLatLongDao.GetUserLatLong(userid);
				ArrayList<LatLongBean> shop_lat_lon_list_beans =shopLatLongDao.GetFeeds();
				
				Distancecalcualation distancecalcualation = new Distancecalcualation();				
				
				ArrayList<String> shopidlist = new ArrayList<String>();
				HashMap<String, Double> dismap = new LinkedHashMap<String, Double>();
				
				//////////////////////////////////////////////////////////
				
				for(int i = 0 ; i<shop_lat_lon_list_beans.size() ; i++){
					
					
					Double dis = 	distancecalcualation.distance(u_lat_lon_list_beans.get(0).getU_lat(), u_lat_lon_list_beans.get(0).getU_lon(), shop_lat_lon_list_beans.get(i).getShop_lat(), shop_lat_lon_list_beans.get(i).getShop_long(), "k");
					
					
					dismap.put(shop_lat_lon_list_beans.get(i).getShopid(), dis);
					
					}
					
					System.out.println(dismap);
					
					SortHasMap sortHasMap = new SortHasMap();
					
					HashMap<String, Double> sorteddismap = sortHasMap.sortByValues(dismap);
					
					System.out.println(sorteddismap);
					
					
					Set<String> keys = sorteddismap.keySet();
			        for(String key: keys){
			            
			            shopidlist.add(key);
			        }
			
			        
			        System.out.println(shopidlist);
					
			        RejectOrderDao rejectOrderDao = new RejectOrderDao();
			        
					String device_id = rejectOrderDao.GetDeviceId(shopidlist.get(0));
				/////////////////////////////////////////////////////////
				GCMAppStandAlone gcmAppStandAlone = new GCMAppStandAlone();
				
				AssignInsertDao assignInsertDao = new AssignInsertDao();
				
				assignInsertDao.assignMasterInsertAssignedBy(orderidStr, shopidlist.get(0), userid, datetime);
				
				gcmAppStandAlone.doSend(orderidStr,datetime,userid,device_id);
				
				////////////////////////////////////////////////////////
				response = Utitlity.constructJSON("ORDER INSERT",true,orderId);
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
				
				
					response = Utitlity.constructJSON(" error",false,"failed");
				
			}
			return response;
					
		}
		
		private int userOrder(ArrayList<Product> product){
			
			int result = 3;
			
				try {
					if(OrderDao.insertOrder(product)){
						System.out.println("RegisterUSer if");
						result = 0;
					}
				} /*catch(SQLException sqle){
					System.out.println("RegisterUSer catch sqle");
					//When Primary key violation occurs that means user is already registered
					if(sqle.getErrorCode() == 1062){
						result = 1;
					} 
					//When special characters are used in name,username or password
					else if(sqle.getErrorCode() == 1064){
						System.out.println(sqle.getErrorCode());
						result = 2;
					}
				}*/
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			
				
			return result;
		}
	
}

