package readymed.sk.webservice;



import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.multipart.FormDataParam;


import readymed.sk.dao.RetailerLoginDao;
import readymed.sk.dto.RetailerLoginBean;
import readymed.sk.utility.Utitlity;





/**
 * @SaurabhKar
 *
 */
@Path("/retailerLogin")
public class RetailerLogin {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/doRetailerLogin")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doLogin(RetailerLoginBean retailerLoginBean) throws SQLException, Exception{
			String response = "";
			
			
			String uname = retailerLoginBean.getUsername();
			String password = retailerLoginBean.getPassword();
			String device_id = retailerLoginBean.getDevice_id();
			
					
			
			/*String uname= registerBean.getUserName();
			String phone = registerBean.getPhone();
			String email = registerBean.getEmail();
			String add = registerBean.getAddress();
			String city = registerBean.getCity();
			String pin = registerBean.getPin();*/
			
			System.out.println("Inside doLogin ::"+uname+" * "+" * "+password+"  *  "+device_id);
			int retCode = registerUser(uname,password,device_id);
			if(retCode == 0){
				
				String retailerId=RetailerLoginDao.getUserId(uname, password);
				
				RetailerLoginDao.updateDevice(device_id, password, uname);
				
				response = Utitlity.constructJSON("login successful",true,retailerId,"Success");
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
				
				
					response = Utitlity.constructJSON("login error",false);
				
			}
			return response;
					
		}
		
		private int registerUser(String uname,String password,String dev_id){
			//System.out.println("Inside doLogin **::"+uname+" * "+" * "+phone+"  *  "+email+" * "+add+"  *  "+city+"  *  "+pin);
			System.out.println("Inside checkCredentials");
			int result = 3;
			if(Utitlity.isNotNull(uname) && Utitlity.isNotNull(password) && Utitlity.isNotNull(dev_id)){
				try {
					if(RetailerLoginDao.ratailLogin(uname,password)){
						System.out.println("RegisterUSer if");
						result = 0;
					}
				} catch(SQLException sqle){
					System.out.println("RegisterUSer catch sqle");
					//When Primary key violation occurs that means user is already registered
					if(sqle.getErrorCode() == 1062){
						result = 1;
					} 
					//When special characters are used in name,username or password
					else if(sqle.getErrorCode() == 1064){
						System.out.println(sqle.getErrorCode());
						result = 2;
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
	
}

