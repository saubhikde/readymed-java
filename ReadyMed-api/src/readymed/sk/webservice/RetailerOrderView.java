package readymed.sk.webservice;



import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import readymed.sk.dao.PrescriptionViewDao;
import readymed.sk.dao.RetailerOrderViewDao;
import readymed.sk.dto.UserBean;
import readymed.sk.dto.ViewOrderBean;
import readymed.sk.model.UserDetailsFetchesModel;
import readymed.sk.transformer.FetchTransform;



import com.sun.jersey.multipart.FormDataParam;

@Path("/orderView")
public class RetailerOrderView {
	@POST
	// Path: http://localhost/<appln-folder-name>/login/dologin
	@Path("/doOrderView")
	@Consumes(MediaType.APPLICATION_JSON)
	// Produces JSON as response
	@Produces(MediaType.APPLICATION_JSON) 
	// Query parameters are parameters: http://localhost/<appln-folder-name>/login/dologin?username=abc&password=xyz
	public JSONObject doLogin(ViewOrderBean userbeans) throws Exception{
		String feeds  = null;
		JSONArray jarray = new JSONArray();
		JSONObject obj1 = new JSONObject();
		System.out.println("This is here");
		try 
		{
		
			
			String uid = userbeans.getOrderid();
			String uuids = userbeans.getUserid();
			System.out.println(uid);
					
			
			//select o.*,c.id,c.discount,c.type from order_master o FULL JOIN coupon_master c ON o.coupon_id =cast(c.id as character varying) where o.id = 145
			
			
			obj1 = RetailerOrderViewDao.GetFeedsTotal(uid);
			
			JSONArray jarray1 = new JSONArray();
			 jarray1 = RetailerOrderViewDao.GetFeeds(uid,uuids);
			 
			 obj1.put("products", jarray1);
			 
			 PrescriptionViewDao prescriptionViewDao = new PrescriptionViewDao();
			 JSONArray jArray2 = prescriptionViewDao.GetPrescription(Integer.parseInt(uid));
			 
			 obj1.put("prescription", jArray2);
		
			 
			 

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("error");
		}
		return obj1;		
	}
}
