package readymed.sk.webservice;





import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.multipart.FormDataParam;

import readymed.sk.dao.AcceptDao;
import readymed.sk.dao.AssignInsertDao;
import readymed.sk.dao.RegisterDao;
import readymed.sk.dao.RejectOrderDao;
import readymed.sk.dao.RetailerPartialAcceptDao;
import readymed.sk.dao.ShopLatLongDao;
import readymed.sk.dao.UserDetailsFetchForRetailer;
import readymed.sk.dto.LatLongBean;
import readymed.sk.dto.Product;
import readymed.sk.dto.RegisterBean;
import readymed.sk.dto.RetailerAcceptBean;
import readymed.sk.dto.UserBean;
import readymed.sk.utility.Distancecalcualation;
import readymed.sk.utility.PushNotificationShopToShop;
import readymed.sk.utility.SortHasMap;
import readymed.sk.utility.Utitlity;





/**
 * @SaurabhKar
 *
 */
@Path("/accept")
public class RetailerAccept {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/doaccept")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doAccept(JSONObject inputJsonObj) throws SQLException, Exception{
			String response = "";
		
			ArrayList<RetailerAcceptBean> acceptLists = new ArrayList<RetailerAcceptBean>();
			
			String uid = (String) inputJsonObj.get("userid"); 
			
			String oid = (String) inputJsonObj.get("orderid");
			
			System.out.println(oid);
			
			String type = (String) inputJsonObj.get("type");
			
			System.out.println(type);
			
			String sid = (String) inputJsonObj.get("shop_id");
			
			System.out.println(sid);
			
			String mdate = (String)inputJsonObj.get("date_time");
			System.out.println(mdate);
			
			JSONArray inner_arr = (JSONArray) inputJsonObj.get("product");
			
			for(int i=0;i<inner_arr.length(); i++){
				JSONObject obj = (JSONObject) inner_arr.get(i);
				System.out.println(obj.get("accept_type"));
				
				RetailerAcceptBean acceptDto = new RetailerAcceptBean();
				
				acceptDto.setUserid(uid);
				acceptDto.setOrderid(oid);
				acceptDto.setType(type);
				acceptDto.setShop_id(sid);
				acceptDto.setDate_time(mdate);
				acceptDto.setProduct_id(obj.get("product_id").toString());
				acceptDto.setAccept_type(obj.get("accept_type").toString());
				
				
				acceptLists.add(acceptDto);
				
			}

			
			
			
			
			
		//	System.out.println("Inside doRegister ::"+uname+" * "+" * "+phone+"  *   * "+add+"  *  "+city+"  *  "+pin);
			int retCode = changeStatus(acceptLists);
			if(retCode == 0){
				
				UserDetailsFetchForRetailer userDetailsFetchForRetailer = new UserDetailsFetchForRetailer();
				
				JSONArray userLists= userDetailsFetchForRetailer.GetFeeds(uid);
				
				JSONObject obj = new JSONObject();
				
				
				obj.put("message", "Successfully accepted");
				obj.put("status", "true");
				obj.put("user_details", userLists);
				
				response = obj.toString();
				
			//	response = Utitlity.constructJSON("Successfully Accepted",true);
				
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
				
					response = Utitlity.constructJSON(" error",false,"failed");
				
			}
			return response;
					
		}
		
		private int changeStatus(ArrayList<RetailerAcceptBean> accpetLists) throws Exception{
			
			System.out.println("Inside checkCredentials");
			int result = 3;
			if(Utitlity.isNotNull(accpetLists.get(0).getOrderid()) && Utitlity.isNotNull(accpetLists.get(0).getShop_id()) && Utitlity.isNotNull(accpetLists.get(0).getType()) && accpetLists.get(0).getType().equalsIgnoreCase("complete")){
				try {
					AcceptDao acceptDao = new AcceptDao();
					if(acceptDao.doaccept(accpetLists)){
						System.out.println("RegisterUSer if");
						result = 0;
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}
			else if(Utitlity.isNotNull(accpetLists.get(0).getOrderid()) && Utitlity.isNotNull(accpetLists.get(0).getShop_id()) && Utitlity.isNotNull(accpetLists.get(0).getType()) && accpetLists.get(0).getType().equalsIgnoreCase("partial")){
				System.out.println("partial");
				
				ArrayList<RetailerAcceptBean> accptlists = new ArrayList<RetailerAcceptBean>();
				ArrayList<RetailerAcceptBean> rejectlists = new ArrayList<RetailerAcceptBean>();
				
				for(int i=0; i<accpetLists.size();i++){
						if(accpetLists.get(i).getAccept_type().equalsIgnoreCase("selected")){
				
							System.out.println("selected---->"+accpetLists.get(i).getProduct_id());
								accptlists.add(accpetLists.get(i));
					
						}
						else if(accpetLists.get(i).getAccept_type().equalsIgnoreCase("unselected")){
							System.out.println("unselected---->"+accpetLists.get(i).getProduct_id());
							rejectlists.add(accpetLists.get(i));
					
						}
					
				}
				
				//calculation for nearest shop
				/////////////////////////////////////////////////////////////
				ShopLatLongDao shopLatLongDao = new ShopLatLongDao();
				ArrayList<LatLongBean> latLongBeanpartShops = shopLatLongDao.GetFeedsPartShop(accpetLists.get(0).getShop_id());
				ArrayList<LatLongBean> latLongBeans = shopLatLongDao.GetFeeds();
				
				
				Distancecalcualation distancecalcualation = new Distancecalcualation();
				
				ArrayList<String> shopidlist = new ArrayList<String>();
				HashMap<String, Double> dismap = new LinkedHashMap<String, Double>();
				
				for(int i = 0 ; i<latLongBeans.size() ; i++){
					
					
				Double dis = 	distancecalcualation.distance(latLongBeanpartShops.get(0).getShop_lat(), latLongBeanpartShops.get(0).getShop_long(), latLongBeans.get(i).getShop_lat(), latLongBeans.get(i).getShop_long(), "k");
				
				
				dismap.put(latLongBeans.get(i).getShopid(), dis);
				
				}
				
				System.out.println(dismap);
				
				SortHasMap sortHasMap = new SortHasMap();
				
				HashMap<String, Double> sorteddismap = sortHasMap.sortByValues(dismap);
				
				System.out.println(sorteddismap);
				
				
				Set<String> keys = sorteddismap.keySet();
		        for(String key: keys){
		            
		            shopidlist.add(key);
		        }
		
		        
		        System.out.println(shopidlist);
				
		        RejectOrderDao rejectOrderDao = new RejectOrderDao();
		        
				String device_id = rejectOrderDao.GetDeviceId(shopidlist.get(1));
				
				PushNotificationShopToShop notificationShopToShop = new PushNotificationShopToShop();
				
			
	        
				RetailerPartialAcceptDao retailerPartialAcceptDao = new RetailerPartialAcceptDao();
				
				try{
					
					int accept_shop_id = Integer.parseInt(accpetLists.get(0).getShop_id());
					
					int key = retailerPartialAcceptDao.doPartialAccept(accptlists, rejectlists, Integer.parseInt(shopidlist.get(1)),accept_shop_id);
					if(key > 0){
						result = 0;
						notificationShopToShop.doSend1(String.valueOf(key),accpetLists.get(0).getOrderid(), accpetLists.get(0).getDate_time(), accpetLists.get(0).getUserid(), device_id);
					}
				}catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
				//////////////////////////////////////////////////////////////
				
					
				
				
		
				
				
				
				
			}
			else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
	
}


