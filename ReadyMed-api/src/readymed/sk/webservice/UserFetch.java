package readymed.sk.webservice;



import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import readymed.sk.dto.UserBean;
import readymed.sk.model.UserDetailsFetchesModel;
import readymed.sk.transformer.FetchTransform;



import com.sun.jersey.multipart.FormDataParam;

@Path("/fetch")
public class UserFetch {
	@POST
	// Path: http://localhost/<appln-folder-name>/login/dologin
	@Path("/dofetch")
	@Consumes(MediaType.APPLICATION_JSON)
	// Produces JSON as response
	@Produces(MediaType.APPLICATION_JSON) 
	// Query parameters are parameters: http://localhost/<appln-folder-name>/login/dologin?username=abc&password=xyz
	public String doLogin(UserBean userbeans){
		String feeds  = null;
		System.out.println("This is here");
		try 
		{
			ArrayList<UserBean> feedData = null;
			UserDetailsFetchesModel projectManager= new UserDetailsFetchesModel();
			String uid = userbeans.getId();
			System.out.println(uid);
					
			feedData = projectManager.GetFeeds(uid);
			feeds=FetchTransform.UserFeed(feedData);
			

		} catch (Exception e)
		{
			System.out.println("error");
		}
		return feeds;		
	}
}
