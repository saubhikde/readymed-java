package readymed.sk.webservice;





import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;

import readymed.sk.dao.AssignInsertDao;
import readymed.sk.dao.RejectOrderDao;
import readymed.sk.dao.ShopLatLongDao;
import readymed.sk.dto.LatLongBean;
import readymed.sk.dto.RejectBean;
import readymed.sk.utility.Distancecalcualation;
import readymed.sk.utility.PushNotificationShopToShop;
import readymed.sk.utility.SortHasMap;
import readymed.sk.utility.Utitlity;







@Path("/reject")
public class RetailerOrderRejection {
	@POST
	// Path: http://localhost/<appln-folder-name>/login/dologin
	@Path("/doReject")
	@Consumes(MediaType.APPLICATION_JSON)
	// Produces JSON as response
	@Produces(MediaType.APPLICATION_JSON) 
	// Query parameters are parameters: http://localhost/<appln-folder-name>/login/dologin?username=abc&password=xyz
	public String doLogin(RejectBean rejectBean) throws Exception{
		String feeds  = null;
		String response = "";
		JSONArray jarray = new JSONArray();
		System.out.println("This is here");
		try 
		{
		
			
			String oid = rejectBean.getOrderid();
			String sid = rejectBean.getShopid();
			String uid = rejectBean.getUserid();
			String date = rejectBean.getDate();
			
			ShopLatLongDao shopLatLongDao = new ShopLatLongDao();
			ArrayList<LatLongBean> latLongBeanpartShops = shopLatLongDao.GetFeedsPartShop(sid);
			ArrayList<LatLongBean> latLongBeans = shopLatLongDao.GetFeeds();
			
			
			Distancecalcualation distancecalcualation = new Distancecalcualation();
			
			ArrayList<String> shopidlist = new ArrayList<String>();
			HashMap<String, Double> dismap = new LinkedHashMap<String, Double>();
			
			for(int i = 0 ; i<latLongBeans.size() ; i++){
				
				
			Double dis = 	distancecalcualation.distance(latLongBeanpartShops.get(0).getShop_lat(), latLongBeanpartShops.get(0).getShop_long(), latLongBeans.get(i).getShop_lat(), latLongBeans.get(i).getShop_long(), "k");
			
			
			dismap.put(latLongBeans.get(i).getShopid(), dis);
			
			}
			
			System.out.println(dismap);
			
			SortHasMap sortHasMap = new SortHasMap();
			
			HashMap<String, Double> sorteddismap = sortHasMap.sortByValues(dismap);
			
			System.out.println(sorteddismap);
			
			
			Set<String> keys = sorteddismap.keySet();
	        for(String key: keys){
	            
	            shopidlist.add(key);
	        }
	
	        
	        System.out.println(shopidlist);
			
	        RejectOrderDao rejectOrderDao = new RejectOrderDao();
	        
			String device_id = rejectOrderDao.GetDeviceId(shopidlist.get(1));
			
			PushNotificationShopToShop notificationShopToShop = new PushNotificationShopToShop();
			
		
            AssignInsertDao assignInsertDao = new AssignInsertDao();
			
			assignInsertDao.assignMasterInsert(oid, shopidlist.get(1), uid, date);
			
			
			notificationShopToShop.doSend(oid, date, uid, device_id);
			
			
			
		    response = Utitlity.constructJSON("Successfully rejected", true);
			

		} catch (Exception e)
		{
			System.out.println("error");
		}
		return response;		
	}
}