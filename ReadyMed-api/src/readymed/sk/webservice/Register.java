package readymed.sk.webservice;



import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.multipart.FormDataParam;

import readymed.sk.dao.RegisterDao;
import readymed.sk.dto.RegisterBean;
import readymed.sk.utility.Utitlity;





/**
 * @SaurabhKar
 *
 */
@Path("/register")
public class Register {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/doregister")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doLogin(RegisterBean registerBean) throws SQLException, Exception{
			String response = "";
			String uname= registerBean.getUserName();
			String phone = registerBean.getPhone();
			String email = registerBean.getEmail();
			String add = registerBean.getAddress();
			String city = registerBean.getCity();
			String pin = registerBean.getPin();
			String c_lat = registerBean.getCust_lat();
			String c_long = registerBean.getCust_long();
			
			Double c_lat_d = Double.parseDouble(c_lat);
			Double c_long_d = Double.parseDouble(c_long);
			
			
		//	System.out.println("Inside doRegister ::"+uname+" * "+" * "+phone+"  *   * "+add+"  *  "+city+"  *  "+pin);
			int retCode = registerUser(uname,phone,email,add,city,pin,c_lat_d,c_long_d);
			if(retCode == 0){
				
				String userId=RegisterDao.getUserId(uname, phone);
				
				response = Utitlity.constructJSON("Successfully Register",true,userId,"Success");
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
				
				Boolean statusUpdate = RegisterDao.updateUser(uname, phone, email, add, city, pin,c_lat_d,c_long_d);
				String userId=RegisterDao.getUserId(uname, phone);
				if(statusUpdate){
				response = Utitlity.constructJSON(" User Successfully updated",statusUpdate, userId,"Success");
				}
				else{
					response = Utitlity.constructJSON(" error",statusUpdate,"failed");
				}
			}
			return response;
					
		}
		
		private int registerUser(String uname,String phone,String email,String add,String city,String pin,Double c_lat_d,Double c_long_d) throws Exception{
			//System.out.println("Inside doLogin **::"+uname+" * "+" * "+phone+"  *  "+email+" * "+add+"  *  "+city+"  *  "+pin+"  * "+c_lat_d+"  *  "+c_long_d);
			System.out.println("Inside checkCredentials");
			int result = 3;
			if(Utitlity.isNotNull(uname) && Utitlity.isNotNull(phone) && Utitlity.isNotNull(add) && Utitlity.isNotNull(city) && Utitlity.isNotNull(pin) && c_lat_d != null && c_long_d != null){
				try {
					if(RegisterDao.insertUser(uname, phone,email,add,city,pin,c_lat_d,c_long_d)){
						System.out.println("RegisterUSer if");
						result = 0;
					}
				} catch(SQLException sqle){
					System.out.println("RegisterUSer catch sqle");
					//When Primary key violation occurs that means user is already registered
					if(sqle.getErrorCode() == 1062){
						result = 1;
					} 
					//When special characters are used in name,username or password
					else if(sqle.getErrorCode() == 1064){
						System.out.println(sqle.getErrorCode());
						result = 2;
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
	
}

