package readymed.sk.webservice;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.multipart.FormDataParam;


import readymed.sk.dao.UserUpdateDao;
import readymed.sk.dto.UpdateUserBean;
import readymed.sk.utility.Utitlity;





/**
 * @SaurabhKar
 *
 */
@Path("/update")
public class Update {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/doupdate")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doLogin(UpdateUserBean updateBean) throws SQLException, Exception{
			String response = "";
			String id = updateBean.getId();
			String uname= updateBean.getUsername();
			
			String email = updateBean.getEmailid();
			String add = updateBean.getAddress();
			String city = updateBean.getCity();
			String pin = updateBean.getPin();
			
			System.out.println("Inside doUpdate ::"+id+" * "+" * "+uname+"  *  "+email+" * "+add+"  *  "+city+"  *  "+pin);
			int retCode = updateUser(id,uname,email,add,city,pin);
			if(retCode == 0){
				
				
				
				response = Utitlity.constructJSON("Successfully Updated",true);
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
				
				/*Boolean statusUpdate = RegisterDao.updateUser(uname, phone, email, add, city, pin);
				String userId=RegisterDao.getUserId(uname, phone);
				if(statusUpdate){
				response = Utitlity.constructJSON(" User Successfully updated",statusUpdate, userId,"Success");
				}
				else{*/
					response = Utitlity.constructJSON(" error",false,"failed");
			//	}
			}
			return response;
					
		}
		
		private int updateUser(String uid,String uname,String email,String add,String city,String pin){
			System.out.println("Inside doUpdateAgain **::"+uid+" * "+" * "+uname+"  *  "+email+" * "+add+"  *  "+city+"  *  "+pin);
			System.out.println("Inside checkCredentials");
			int result = 3;
			if(Utitlity.isNotNull(uname) && Utitlity.isNotNull(uid) && Utitlity.isNotNull(add) && Utitlity.isNotNull(city) && Utitlity.isNotNull(pin)){
				try {
					if(UserUpdateDao.updateUser(uid, uname, email, add, city, pin)){
						System.out.println("RegisterUSer if");
						result = 0;
					}
				} /*catch(SQLException sqle){
					System.out.println("RegisterUSer catch sqle");
					//When Primary key violation occurs that means user is already registered
					if(sqle.getErrorCode() == 1062){
						result = 1;
					} 
					//When special characters are used in name,username or password
					else if(sqle.getErrorCode() == 1064){
						System.out.println(sqle.getErrorCode());
						result = 2;
					}
				}*/
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
	
}

