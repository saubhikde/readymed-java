package readymed.sk.webservice;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.multipart.FormDataParam;


import readymed.sk.dao.DeliveryDao;
import readymed.sk.dao.UserUpdateDao;
import readymed.sk.dto.DeliveryBean;
import readymed.sk.dto.UpdateUserBean;
import readymed.sk.utility.Utitlity;





/**
 * @SaurabhKar
 *
 */
@Path("/delivered")
public class CompleteApi {
	// HTTP Get Method
		@POST 
		// Path: http://localhost/<appln-folder-name>/register/doregister
		@Path("/dodelivered")  
		
		@Consumes(MediaType.APPLICATION_JSON)
		// Produces JSON as response
		@Produces(MediaType.APPLICATION_JSON) 
		// Query parameters are parameters: http://localhost/<appln-folder-name>/register/doregister?name=pqrs&username=abc&password=xyz
		public String doLogin(DeliveryBean deliveryBean ) throws SQLException, Exception{
			String response = "";
			String oid = deliveryBean.getOrder_id();
			String sid= deliveryBean.getShop_id();
			
			
			
			System.out.println("Inside doUpdate ::"+oid+" * "+" * "+sid);
			int retCode = updateStatus(oid,sid);
			if(retCode == 0){
				
				
				
				response = Utitlity.constructJSON("Successfully Delivered",true);
			}else if(retCode == 1){
				response = Utitlity.constructJSON("Not Registered",false, "You are already registered");
			}else if(retCode == 2){
				response = Utitlity.constructJSON("Not Registered",false, "Special Characters are not allowed in Username and Password");
			}else if(retCode == 3){
				
				/*Boolean statusUpdate = RegisterDao.updateUser(uname, phone, email, add, city, pin);
				String userId=RegisterDao.getUserId(uname, phone);
				if(statusUpdate){
				response = Utitlity.constructJSON(" User Successfully updated",statusUpdate, userId,"Success");
				}
				else{*/
					response = Utitlity.constructJSON(" error",false);
			//	}
			}
			return response;
					
		}
		
		private int updateStatus(String oid,String sid){
		
			System.out.println("Inside checkCredentials");
			int result = 3;
			if(Utitlity.isNotNull(oid) && Utitlity.isNotNull(sid)){
				try {
					if(DeliveryDao.updateStatus(oid,sid)){
						System.out.println("RegisterUSer if");
						result = 0;
					}
				} /*catch(SQLException sqle){
					System.out.println("RegisterUSer catch sqle");
					//When Primary key violation occurs that means user is already registered
					if(sqle.getErrorCode() == 1062){
						result = 1;
					} 
					//When special characters are used in name,username or password
					else if(sqle.getErrorCode() == 1064){
						System.out.println(sqle.getErrorCode());
						result = 2;
					}
				}*/
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Inside checkCredentials catch e ");
					result = 3;
				}
			}else{
				System.out.println("Inside checkCredentials else");
				result = 3;
			}
				
			return result;
		}
	
}

