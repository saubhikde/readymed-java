package readymed.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CouponBean {
	
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
	
	
}
