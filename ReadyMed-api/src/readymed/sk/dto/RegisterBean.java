package readymed.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class RegisterBean {
	private String userName;
	private String phone;
	private String email;
	private String address;
	private String city;
	private String pin;
	private String cust_lat;
	private String cust_long;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getCust_lat() {
		return cust_lat;
	}
	public void setCust_lat(String cust_lat) {
		this.cust_lat = cust_lat;
	}
	public String getCust_long() {
		return cust_long;
	}
	public void setCust_long(String cust_long) {
		this.cust_long = cust_long;
	}
	
	

}
