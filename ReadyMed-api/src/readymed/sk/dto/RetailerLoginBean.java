package readymed.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class RetailerLoginBean {
	
	
	private String username;
	private String password;
	private String device_id;
	private String id;
	
	
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	

}
