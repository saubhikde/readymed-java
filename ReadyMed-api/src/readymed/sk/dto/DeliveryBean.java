package readymed.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DeliveryBean {

	
	String order_id;
	String shop_id;
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getShop_id() {
		return shop_id;
	}
	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}
	
	
	
}
