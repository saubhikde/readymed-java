package readymed.sk.dto;

public class LatLongBean {

	private String shopid;
	private double shop_lat;
	private double shop_long;
	
	public String getShopid() {
		return shopid;
	}
	public void setShopid(String shopid) {
		this.shopid = shopid;
	}
	public double getShop_lat() {
		return shop_lat;
	}
	public void setShop_lat(double shop_lat) {
		this.shop_lat = shop_lat;
	}
	public double getShop_long() {
		return shop_long;
	}
	public void setShop_long(double shop_long) {
		this.shop_long = shop_long;
	}
	
	
	
	
	
}
