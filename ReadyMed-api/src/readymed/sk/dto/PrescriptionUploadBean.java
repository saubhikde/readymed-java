package readymed.sk.dto;

public class PrescriptionUploadBean {
	
	private String order_id;
	private String pres_url;
	
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	
	public String getPres_url() {
		return pres_url;
	}
	public void setPres_url(String pres_url) {
		this.pres_url = pres_url;
	}
	
}
