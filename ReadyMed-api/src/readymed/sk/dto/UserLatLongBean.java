package readymed.sk.dto;

public class UserLatLongBean {
	
	private String uid;
	private Double u_lat;
	private Double u_lon;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Double getU_lat() {
		return u_lat;
	}
	public void setU_lat(Double u_lat) {
		this.u_lat = u_lat;
	}
	public Double getU_lon() {
		return u_lon;
	}
	public void setU_lon(Double u_lon) {
		this.u_lon = u_lon;
	}
	
	
	

}
