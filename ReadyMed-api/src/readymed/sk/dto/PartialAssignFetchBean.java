package readymed.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PartialAssignFetchBean {
	
	
	private String aid;
	

	private String oid;
	private String uid;
	
	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}
	
	

}
