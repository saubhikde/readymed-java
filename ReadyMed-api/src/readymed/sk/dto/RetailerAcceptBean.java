package readymed.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class RetailerAcceptBean {

	private String userid;
	private String orderid;
	private String type;
	private String shop_id;
	private String product_id;
    private String accept_type;
    private String date_time;
    
    
    
    
	
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getShop_id() {
		return shop_id;
	}
	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}
	public String getAccept_type() {
		return accept_type;
	}
	public void setAccept_type(String accept_type) {
		this.accept_type = accept_type;
	}
	
	
	
}
