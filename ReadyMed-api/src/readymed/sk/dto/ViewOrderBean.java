package readymed.sk.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ViewOrderBean {

	private String orderid;
	private String userid;
	
	

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	
	
	
}
