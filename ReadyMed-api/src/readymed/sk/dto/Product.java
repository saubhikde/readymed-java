package readymed.sk.dto;

public class Product {
	String user_id;
	String date_time;
	String cartamount;
	String cartDiscount;
	String netcartamount;
	String coupon_id;
	String coupon_discount;
	String tot_net_amount;
	String prod_id;
    String amount;
    String quantity;
    String dis_amount;
    String net_amount;
	
	public String getCoupon_id() {
		return coupon_id;
	}
	public void setCoupon_id(String coupon_id) {
		this.coupon_id = coupon_id;
	}
	public String getCoupon_discount() {
		return coupon_discount;
	}
	public void setCoupon_discount(String coupon_discount) {
		this.coupon_discount = coupon_discount;
	}
	public String getTot_net_amount() {
		return tot_net_amount;
	}
	public void setTot_net_amount(String tot_net_amount) {
		this.tot_net_amount = tot_net_amount;
	}
	
	
   
	public String getCartamount() {
		return cartamount;
	}
	public void setCartamount(String cartamount) {
		this.cartamount = cartamount;
	}
	public String getCartDiscount() {
		return cartDiscount;
	}
	public void setCartDiscount(String cartDiscount) {
		this.cartDiscount = cartDiscount;
	}
	public String getNetcartamount() {
		return netcartamount;
	}
	public void setNetcartamount(String netcartamount) {
		this.netcartamount = netcartamount;
	}
	
	
    public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
    
    public String getProd_id() {
		return prod_id;
	}
	public void setProd_id(String prod_id) {
		this.prod_id = prod_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getDis_amount() {
		return dis_amount;
	}
	public void setDis_amount(String dis_amount) {
		this.dis_amount = dis_amount;
	}
	public String getNet_amount() {
		return net_amount;
	}
	public void setNet_amount(String net_amount) {
		this.net_amount = net_amount;
	}
    
    
    
    
}
