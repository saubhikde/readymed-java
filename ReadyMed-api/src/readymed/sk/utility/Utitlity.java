package readymed.sk.utility;

import java.util.ArrayList;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;



public class Utitlity {
	/**
	 * Null check Method
	 * 
	 * @param txt
	 * @return
	 */
	public static boolean isNotNull(String txt) {
		// System.out.println("Inside isNotNull");
		return txt != null && txt.trim().length() >= 0 ? true : false;
	}

	/**
	 * Method to construct JSON
	 * 
	 * @param tag
	 * @param status
	 * @return
	 */
	
	public static String constructJSON(String tag, boolean status) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("message", tag);
			obj.put("status", new Boolean(status));
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		return obj.toString();
	}
	
	
	public static String constructJSON(String tag, boolean status,String userid,String result) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("message", tag);
			obj.put("status", new Boolean(status));
			obj.put("userid", userid);
			obj.put("result", result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		return obj.toString();
	}
	
	public static String constructJSON(String msg, boolean status,int orderid) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("message", msg);
			obj.put("status", new Boolean(status));
			obj.put("orderid", orderid);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		return obj.toString();
	}

	/**
	 * Method to construct JSON with Error Msg
	 * 
	 * @param tag
	 * @param status
	 * @param err_msg
	 * @return
	 */
	public static String constructJSON(String tag, boolean status,String err_msg) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("message", tag);
			obj.put("status", new Boolean(status));
			obj.put("error_msg", err_msg);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		return obj.toString(); 
	}
	
	
	/*public static String constructJSON(ArrayList<StateBean> stateBean) {
		JSONObject obj = new JSONObject();
		try {
		
			for(int i = 0;i<stateBean.size();i++){
			obj.put("ID", stateBean.get(0).getId());
			obj.put("NAME", stateBean.get(0).getState_name());
			obj.put("CAPITAL", stateBean.get(0).getCapital());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		return obj.toString(); 
	}*/
	
}
