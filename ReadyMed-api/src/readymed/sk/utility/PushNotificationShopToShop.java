package readymed.sk.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;*/

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

public class PushNotificationShopToShop {
private static final long serialVersionUID = 1L;
	
	// The SENDER_ID here is the "Browser Key" that was generated when I
	// created the API keys for my Google APIs project.
	private static final String SENDER_ID = "AIzaSyCTCKSxZTDhrIWvi4uIg8cah2nq5K_hlYM";
	
	// This is a *cheat*  It is a hard-coded registration ID from an Android device
	// that registered itself with GCM using the same project id shown above.
	//private static final String ANDROID_DEVICE = "APA91bE-bvQe1uQR5VVi64mNMREmSKYdNa5tr-yxO-WOeywJnMBDxIVj248g41pjCetHHB7rfz3-PmSOQlorSA_Q5AseS_r-kMRrnIPODnjBmqXUroYpGUjBh3I9AOM_yt2GvFBxUzVP1e84lpwyuBuM0FU64X0zCg";
		
	// This array will hold all the registration ids used to broadcast a message.
	// for this demo, it will only have the ANDROID_DEVICE id that was captured 
	// when we ran the Android client app through Eclipse.
	private List<String> androidTargets = new ArrayList<String>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public  PushNotificationShopToShop() {
    	
        super();

        // we'll only add the hard-coded *cheat* target device registration id 
        // for this demo.
        
        
    }
    
    public void doSend(String orderid,String date,String userid,String ANDROID_DEVICE) throws Exception {
		
		// We'll collect the "CollapseKey" and "Message" values from our JSP page
		String collapseKey = "";
		String userMessage = "";
		
		try {
			userMessage = orderid;
			collapseKey = "GCM_Message";
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		// Instance of com.android.gcm.server.Sender, that does the
		// transmission of a Message to the Google Cloud Messaging service.
		Sender sender = new Sender(SENDER_ID);
		
		androidTargets.clear();
		
		androidTargets.add(ANDROID_DEVICE);
		// This Message object will hold the data that is being transmitted
		// to the Android client devices.  For this demo, it is a simple text
		// string, but could certainly be a JSON object.
		Message message = new Message.Builder()
		
		// If multiple messages are sent using the same .collapseKey()
		// the android target device, if it was offline during earlier message
		// transmissions, will only receive the latest message for that key when
		// it goes back on-line.
		.collapseKey(collapseKey)
		.timeToLive(30)
		.delayWhileIdle(true)
		.addData("orderid", userMessage)
		.addData("message", "You have received a new order")
		.addData("date_time", date)
		.addData("user_id", userid)
		.build();
		
		try {
			// use this for multicast messages.  The second parameter
			// of sender.send() will need to be an array of register ids.
			MulticastResult result = sender.send(message, androidTargets, 1);
			
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				System.out.println("Success"+result.getResults());
				if (canonicalRegId != 0) {
					
				}
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}
			
			androidTargets.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		// We'll pass the CollapseKey and Message values back to index.jsp, only so
		// we can display it in our form again.
		/*request.setAttribute("CollapseKey", collapseKey);
		request.setAttribute("Message", userMessage);
		
		request.getRequestDispatcher("index.jsp").forward(request, response);*/
				
	}
    
public void doSend1(String assignid,String orderid,String date,String userid,String ANDROID_DEVICE) throws Exception {
		
		// We'll collect the "CollapseKey" and "Message" values from our JSP page
		String collapseKey = "";
		String userMessage = "";
		
		try {
			userMessage = assignid;
			collapseKey = "GCM_Message";
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		// Instance of com.android.gcm.server.Sender, that does the
		// transmission of a Message to the Google Cloud Messaging service.
		Sender sender = new Sender(SENDER_ID);
		
		androidTargets.clear();
		
		androidTargets.add(ANDROID_DEVICE);
		// This Message object will hold the data that is being transmitted
		// to the Android client devices.  For this demo, it is a simple text
		// string, but could certainly be a JSON object.
		Message message = new Message.Builder()
		
		// If multiple messages are sent using the same .collapseKey()
		// the android target device, if it was offline during earlier message
		// transmissions, will only receive the latest message for that key when
		// it goes back on-line.
		.collapseKey(collapseKey)
		.timeToLive(30)
		.delayWhileIdle(true)
		.addData("assignid", userMessage)
		.addData("orderid", orderid)
		.addData("message", "You have received a new order")
		.addData("date_time", date)
		.addData("user_id", userid)
		.build();
		
		try {
			// use this for multicast messages.  The second parameter
			// of sender.send() will need to be an array of register ids.
			MulticastResult result = sender.send(message, androidTargets, 1);
			
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				System.out.println("Success"+result.getResults());
				if (canonicalRegId != 0) {
					
				}
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}
			
			androidTargets.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		// We'll pass the CollapseKey and Message values back to index.jsp, only so
		// we can display it in our form again.
		/*request.setAttribute("CollapseKey", collapseKey);
		request.setAttribute("Message", userMessage);
		
		request.getRequestDispatcher("index.jsp").forward(request, response);*/
				
	}
    

    


}
