package readymed.sk.utility;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	@SuppressWarnings("finally")
	public Connection createConnection() throws Exception {
		Connection con = null;
		try {
			System.out.println("Primary Connection ");	
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection("jdbc:postgresql://postgresql-frankross.c9kucc6hwg6h.us-west-2.rds.amazonaws.com:5432/readymed","postgres","postgres");
			
		} catch (Exception e) {
			throw e;
		} finally {
			return con;
		}
	}
}
