package readymed.sk.model;

import java.sql.Connection;
import java.util.ArrayList;

import readymed.sk.dao.UserDetailsFetchesDao;
import readymed.sk.dto.UserBean;
import readymed.sk.utility.Database;



public class UserDetailsFetchesModel {
	public ArrayList<UserBean> GetFeeds(String User_Id)throws Exception {
		System.out.println("This is under project manager");
		ArrayList<UserBean> feeds = null;
		try {
			    Database database= new Database();
			    Connection connection = database.Get_Connection();
			    UserDetailsFetchesDao project= new UserDetailsFetchesDao();
				feeds=project.GetFeeds(connection,User_Id);
		
		} catch (Exception e) {
			throw e;
		}
		return feeds;
	}
}
