package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import readymed.sk.dto.RetailerAcceptBean;
import readymed.sk.utility.Database;

public class AcceptDao {
	
	public static boolean doaccept(ArrayList<RetailerAcceptBean> accptlist) throws SQLException, Exception {
		boolean updateStatus = false;
		Connection dbConn = null;
		String id = null;
		System.out.println("In u Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("update assign_master set current_status= 'complete accepted' where shop_id=? and order_id = ?");
		
			ps.setInt(1,Integer.parseInt(accptlist.get(0).getShop_id()));
			ps.setInt(2, Integer.parseInt(accptlist.get(0).getOrderid()));
			
			
			
			int record = ps.executeUpdate();
			if (record > 0) {
				updateStatus = true;
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return updateStatus;
	}
	

}
