package readymed.sk.dao;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import readymed.sk.utility.Database;

public class AssignInsertDao {

	public static void assignMasterInsert(String oid,String sid,String uid,String adate) throws Exception{
		Connection dbConn = null;
		System.out.println("In assin master Dao");
		try {
			
				Database database = new Database();
				dbConn = database.Get_Connection();
			
			
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
			String dateInString = adate;
			 
			
		 
				Date utildate = formatter.parse(dateInString);
				System.out.println(utildate);
				System.out.println(formatter.format(utildate));
		 
			
			java.sql.Timestamp sqlDate = new java.sql.Timestamp(utildate.getTime());
			System.out.println("-->"+sqlDate);
			
			
		
			
			String a_type = "complete";
			
			String query = "INSERT into assign_master(order_id,shop_id,user_id,assign_date,assign_type) values('"+oid+ "',"+"'"
					+ sid + "','" + uid + "','" +sqlDate+ "','"+a_type+ "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
			boolean	insertStatus = true;
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		}  finally {
			if (dbConn != null) {
				dbConn.close();
			//	dbConn.commit();
			}
		}
	}
	
	public void assignMasterInsertAssignedBy(String oid,String sid,String uid,String adate) throws Exception{
		Connection dbConn = null;
		System.out.println("In assin master Dao");
		try {
			
				Database database = new Database();
				dbConn = database.Get_Connection();
			
			
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
			String dateInString = adate;
			 
			
		 
				Date utildate = formatter.parse(dateInString);
				System.out.println(utildate);
				System.out.println(formatter.format(utildate));
		 
			
			java.sql.Timestamp sqlDate = new java.sql.Timestamp(utildate.getTime());
			System.out.println("-->"+sqlDate);
			
			
			
			
			String a_type = "complete";
			String a_by = "customer";
		//	Double c_discount = Double.parseDouble(c_dis);
			
			String query = "INSERT into assign_master(order_id,shop_id,user_id,assign_date,assign_type,assigned_by) values('"+oid+ "',"+"'"
					+ sid + "','" + uid + "','" +sqlDate+ "','"+a_type+"','"+a_by+ "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
			boolean	insertStatus = true;
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		}  finally {
			if (dbConn != null) {
				dbConn.close();
			//	dbConn.commit();
			}
		}
	}
	
	
}
