package readymed.sk.dao;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;


import readymed.sk.utility.Database;

public class CouponFetchDao {

	public static JSONArray GetFeeds() throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONArray jsonArray = new JSONArray();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			
        	String query = "select code from coupon_master" ;
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			 
		        while (rs.next()) {
		            int total_rows = rs.getMetaData().getColumnCount();
		            JSONObject obj = new JSONObject();
		            for (int i = 0; i < total_rows; i++) {
		                obj.put("text", "use coupon code "+rs.getObject(i + 1)+" to get 15% off");
		            }
		            
		            obj.put("status", "true");
		            jsonArray.put(obj);
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return jsonArray;
	}
	
	
	public static JSONArray GetFeedsAllbycouCode(String code) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONArray jsonArray = new JSONArray();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			
        	String query = "select id,type,discount,exp_date from coupon_master where code = '"+code+"'" ;
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			 
			while (rs.next()) {
	            int total_rows = rs.getMetaData().getColumnCount();
	            JSONObject obj = new JSONObject();
	            for (int i = 0; i < total_rows; i++) {
	                obj.put(rs.getMetaData().getColumnLabel(i + 1)
	                        .toLowerCase(), rs.getObject(i + 1));
	            }
	            
	            obj.put("status", "true");
	            
	            jsonArray.put(obj);
	        }
		            
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return jsonArray;
	}
	
	
	
}
