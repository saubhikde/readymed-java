package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import readymed.sk.dto.LatLongBean;
import readymed.sk.dto.UserBean;
import readymed.sk.utility.Database;

public class ShopLatLongDao {
	public ArrayList<LatLongBean> GetFeeds() throws Exception
	{
		
		Connection dbConn = null;
		System.out.println("fetching lat long");
		ArrayList<LatLongBean> feedData = new ArrayList<LatLongBean>();
		try
		{
			
				Database database = new Database();
				dbConn = database.Get_Connection();
			

			
			PreparedStatement ps = dbConn.prepareStatement("SELECT * FROM shop_master ");
			
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				LatLongBean feedObject = new LatLongBean();
				
				feedObject.setShop_lat(rs.getDouble("shop_lat"));
				feedObject.setShop_long(rs.getDouble("shop_long"));
				feedObject.setShopid(rs.getString("id"));
				
				
				
				feedData.add(feedObject);
			}
			return feedData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public ArrayList<LatLongBean> GetFeedsPartShop(String shop_ID) throws Exception
	{
		
		Connection dbConn = null;
		System.out.println("fetching lat long");
		ArrayList<LatLongBean> feedData = new ArrayList<LatLongBean>();
		try
		{
			
				Database database = new Database();
				dbConn = database.Get_Connection();
			

			int sid = Integer.parseInt(shop_ID);
			PreparedStatement ps = dbConn.prepareStatement("SELECT * FROM shop_master where id= "+ sid);
		
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				LatLongBean feedObject = new LatLongBean();
				
				feedObject.setShop_lat(rs.getDouble("shop_lat"));
				feedObject.setShop_long(rs.getDouble("shop_long"));
				feedObject.setShopid(rs.getString("id"));
				
				
				
				feedData.add(feedObject);
			}
			return feedData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
}

