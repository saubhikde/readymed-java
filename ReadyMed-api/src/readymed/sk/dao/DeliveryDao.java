package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import readymed.sk.utility.Database;

public class DeliveryDao {
	public static boolean updateStatus(String oid,String sid) throws Exception{
		
		boolean updateStatus = false;
		Connection dbConn = null;
		System.out.println("In update Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int orderid = Integer.parseInt(oid);
			int shopid = Integer.parseInt(sid);
			
			PreparedStatement ps = dbConn.prepareStatement(" update assign_master set walk_status='delivered' where  id = (select min(id) from assign_master where order_id=? and shop_id= ?)");
			ps.setInt(1, orderid);
			ps.setInt(2,shopid);
			
			
			int record = ps.executeUpdate();
			if (record > 0) {
				updateStatus = true;
			}
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return updateStatus;
	}
}
