package readymed.sk.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import readymed.sk.utility.Database;



public class PrescriptionUploadDao {

	
	public static boolean insertImageUrl(String order_id,String add,String url) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int oid = Integer.parseInt(order_id);
			
			String query = "INSERT into order_prescription(order_id,image_add,image_url) values('"+oid+ "',"+"'"
			
					+ add +"',"+"'"+url+ "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
				insertStatus = true;
			}
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return insertStatus;
	}
}
