package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import readymed.sk.dto.LatLongBean;
import readymed.sk.utility.Database;

public class RejectOrderDao {
	public String GetDeviceId(String shop_ID) throws Exception
	{
		String device_id = "";
		Connection dbConn = null;
		System.out.println("fetching lat long");
		ArrayList<LatLongBean> feedData = new ArrayList<LatLongBean>();
		try
		{
			
				Database database = new Database();
				dbConn = database.Get_Connection();
			

			int sid = Integer.parseInt(shop_ID);
			PreparedStatement ps = dbConn.prepareStatement("SELECT * FROM shop_master where id= "+ sid);
		
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			 device_id = rs.getString("device_id");
			}
			
		}
		catch(Exception e)
		{
			throw e;
		}
		return device_id;
	}
}
