package readymed.sk.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import readymed.sk.dto.Product;
import readymed.sk.utility.Database;

public class OrderDao {
	public static boolean insertOrder(ArrayList<Product> product) throws SQLException, Exception {
		
		System.out.println("Hiii");
		boolean insertStatus = false;
		
		orderMasterInsert(product);
		
		
		int orderIdforAll = mgetOrderId(product);
		System.out.println("==>"+orderIdforAll);
		
		if(order_product_entry(product, orderIdforAll)){
			insertStatus = true;
		}
		/*Connection dbConn = null;
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String query = "INSERT into user_master(user_name,mobile_no, email_id,address,city,pin) values('"+uname+ "',"+"'"
					+ phone + "','" + email + "','" +address+ "','"+city+"','"+pin+ "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
				insertStatus = true;
			}
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}*/
		return insertStatus;
	}
	
	
	public static void orderMasterInsert(ArrayList<Product> product) throws Exception{
		Connection dbConn = null;
		System.out.println("In Register Dao");
		try {
			
				Database database = new Database();
				dbConn = database.Get_Connection();
			
			
			Double cartamount = Double.parseDouble(product.get(0).getCartamount());
			System.out.println("-->"+cartamount);
			Double cartdiscount = Double.parseDouble(product.get(0).getCartDiscount());
			System.out.println("-->"+cartdiscount);
			Double netcartamount = Double.parseDouble(product.get(0).getNetcartamount());
			System.out.println("-->"+netcartamount);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
			String dateInString = product.get(0).getDate_time();
			 
			
		 
				Date utildate = formatter.parse(dateInString);
				System.out.println(utildate);
				System.out.println(formatter.format(utildate));
		 
			
			java.sql.Timestamp sqlDate = new java.sql.Timestamp(utildate.getTime());
			System.out.println("-->"+sqlDate);
			int userid = Integer.parseInt(product.get(0).getUser_id());
			System.out.println("-->"+userid);
			
			/*System.out.println("-->"+cartamount);
			System.out.println("-->"+cartdiscount);
			System.out.println("-->"+netcartamount);
			System.out.println("-->"+sqlDate);
			System.out.println("-->"+userid);*/
			
		String c_id = product.get(0).getCoupon_id();
			String c_dis = product.get(0).getCoupon_discount();
			String t_n_amt = product.get(0).getTot_net_amount();
			
			String query = "INSERT into order_master(cart_amount,cart_discount,net_cart_amount,order_date,user_id,coupon_id,coupon_discount,tot_net_amount) values('"+cartamount+ "',"+"'"
					+ cartdiscount + "','" + netcartamount + "','" +sqlDate+ "','"+userid+"','"+c_id+"','"+c_dis+"','"+t_n_amt+ "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
			boolean	insertStatus = true;
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		}  finally {
			if (dbConn != null) {
				dbConn.close();
			//	dbConn.commit();
			}
		}
	}
	
	public static int mgetOrderId(ArrayList<Product> product) throws Exception{
		int orderid = 0;
		///////////////////////
		
		boolean insertStatus = false;
		Connection dbConn = null;
		
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("SELECT id FROM order_master WHERE user_id=? and order_date = ? ");
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
			String dateInString = product.get(0).getDate_time();
			 
			
		 
				Date utildate = formatter.parse(dateInString);
				System.out.println(utildate);
				System.out.println(formatter.format(utildate));
		 
			
			java.sql.Timestamp sqlDate = new java.sql.Timestamp(utildate.getTime());
			System.out.println("-->"+sqlDate);
			
			
			
			int userid = Integer.parseInt(product.get(0).getUser_id());
			
			ps.setInt(1,userid);
			ps.setTimestamp(2, sqlDate);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			orderid= rs.getInt("id");
			
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		
		System.out.println(orderid);
		///////////////////////
		return orderid;
	}
	
	
	public static boolean order_product_entry(ArrayList<Product> product,int orderid) throws Exception{
		int effected_row =0;
		///////////////////////////////////////////////////////////////////////////
		boolean returnStatus = false;
		Connection dbConn = null;
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("==>"+product.size());
			
			for(int i=0;i < product.size();i++){
			
				
				
			int order = orderid;	
			int producId = Integer.parseInt(product.get(i).getProd_id());
			System.out.println("===>>>"+producId);
			int userid = Integer.parseInt(product.get(i).getUser_id());
			Double prodamount = Double.parseDouble(product.get(i).getAmount());
			Double proddiscount = Double.parseDouble(product.get(i).getDis_amount());
			Double netprodamount = Double.parseDouble(product.get(i).getNet_amount());
			String quantity =product.get(i).getQuantity();
			
			
			 
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
			String dateInString = product.get(i).getDate_time();
			 
			
		 
				Date utildate = formatter.parse(dateInString);
				System.out.println(utildate);
				System.out.println(formatter.format(utildate));
		 
			
			java.sql.Timestamp sqlDate = new java.sql.Timestamp(utildate.getTime());
			System.out.println("-->"+sqlDate);
			
			
			
			
			
			String query = "INSERT into order_user_product_bind(order_id,product_id,user_id,product_amount,product_discount,net_product_amount,quantity,date_time) values('"+order+ "',"+"'"
					+ producId + "','" + userid + "','" +prodamount+ "','"+proddiscount+ "','" +netprodamount+ "','" +quantity+ "','" +sqlDate+ "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
			boolean	insertStatus = true;
			}
			}
			
			 returnStatus = true;
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
				
			}
		}
		
		
		///////////////////////////////////////////////////////////////////////////
		return returnStatus;
	}
	
	public static Double mgetOrderCouponDiscount(int oid) throws Exception{
		Double coup_dis = 0.00;
		String coup_dis_str="";
		///////////////////////
		
		boolean insertStatus = false;
		Connection dbConn = null;
		
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("SELECT coupon_discount FROM order_master WHERE id=?");
						
			ps.setInt(1,oid);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			coup_dis_str = rs.getString("coupon_discount");
			
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		
	coup_dis = Double.parseDouble(coup_dis_str);
		///////////////////////
		return coup_dis;
	}
	
	
	public static int mgetlineCount(int oid) throws Exception{
		
		int count = 0;
		///////////////////////
		
		boolean insertStatus = false;
		Connection dbConn = null;
		
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("select count(product_id) from order_user_product_bind where order_id = ?");
						
			ps.setInt(1,oid);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			count = rs.getInt(1);
			
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		
	
		///////////////////////
		return count;
	}
	
	
}
