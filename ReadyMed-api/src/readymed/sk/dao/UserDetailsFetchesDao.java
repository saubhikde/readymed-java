package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import readymed.sk.dto.UserBean;


public class UserDetailsFetchesDao {
	public ArrayList<UserBean> GetFeeds(Connection connection,String User_ID) throws Exception
	{
		System.out.println("Oh yes! this is under dao "+User_ID+" ** ");
		ArrayList<UserBean> feedData = new ArrayList<UserBean>();
		try
		{
			int userid = Integer.parseInt(User_ID);
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM user_master WHERE id=?");
			ps.setInt(1,userid);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				UserBean feedObject = new UserBean();
				
				feedObject.setUser_name(rs.getString("user_name"));
				feedObject.setMobileno(rs.getString("mobile_no"));
				feedObject.setEmail(rs.getString("email_id"));
				feedObject.setAddress(rs.getString("address"));
				feedObject.setCity(rs.getString("city"));
				feedObject.setPin(rs.getString("pin"));
				
				/*feedObject.setPatient_mob(rs.getString("patient_mob"));
				feedObject.setPatient_name(rs.getString("patient_name"));*/
				/*feedObject.setMsg_id(rs.getString("msg_id"));
				feedObject.setMessage(rs.getString("message"));
				feedObject.setUser_id(rs.getString("user_id_fk"));*/
				feedData.add(feedObject);
			}
			return feedData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
}
