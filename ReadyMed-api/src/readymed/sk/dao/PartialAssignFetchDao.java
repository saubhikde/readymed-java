package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import readymed.sk.utility.Database;

public class PartialAssignFetchDao {
		
	
	public static JSONArray GetFeeds(String aid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONArray jsonArray = new JSONArray();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			
        	String query = "select l.assign_id,l.order_id,l.product_id,o.user_id,p.post_title,ob.product_amount,ob.product_discount,ob.net_product_amount,ob.quantity from line_assign_master l,product p,order_master o,order_user_product_bind ob where cast(l.product_id as character varying) = p.prod_id and  l.order_id = o.id and l.order_id = ob.order_id and l.product_id = ob.product_id and assign_id = '"+aid+"'" ;
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            int total_rows = rs.getMetaData().getColumnCount();
		            JSONObject obj = new JSONObject();
		            for (int i = 0; i < total_rows; i++) {
		                obj.put(rs.getMetaData().getColumnLabel(i + 1)
		                        .toLowerCase(), rs.getObject(i + 1));
		            }
		            jsonArray.put(obj);
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return jsonArray;
	}
	
	public static JSONObject GetFeedsCoupon(int oid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONObject obj = new JSONObject();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			String query = "select c.id,c.discount,c.type as coupon_type from order_master o FULL JOIN coupon_master c ON o.coupon_id =cast(c.id as character varying) where o.id ="+oid;
        	// String query = "select * from order_master where id= '" +uid+"'" ;
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            
		        	
		            String c_dis_amt = rs.getString("discount");
		            obj.put("coupon_discount_amount",c_dis_amt);
		            obj.put("coupon_type",rs.getString("coupon_type"));
		            
		           
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return obj;
	}
	
	
	//select l.*,p.post_title from line_assign_master l,product p where cast(l.product_id as character varying) = p.prod_id and assign_id = 112;
}
