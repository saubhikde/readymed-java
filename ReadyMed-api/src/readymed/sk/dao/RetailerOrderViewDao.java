package readymed.sk.dao;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import readymed.sk.utility.Database;


public class RetailerOrderViewDao {
	public static JSONArray GetFeeds(String uid,String uuid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONArray jsonArray = new JSONArray();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			
        	String query = "select o.order_id,o.product_id,o.user_id,o.product_amount,o.product_discount,o.net_product_amount,o.quantity,p.post_title from order_user_product_bind o,product p where cast(o.product_id as character varying) = p.prod_id and o.order_id = '" +uid+"'"+"and o.user_id='"+uuid+"'"  ;
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            int total_rows = rs.getMetaData().getColumnCount();
		            JSONObject obj = new JSONObject();
		            for (int i = 0; i < total_rows; i++) {
		                obj.put(rs.getMetaData().getColumnLabel(i + 1)
		                        .toLowerCase(), rs.getObject(i + 1));
		            }
		            jsonArray.put(obj);
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return jsonArray;
	}
	
	public static JSONObject GetFeedsTotal(String uid) throws Exception
	{
		Connection connection = null;
		System.out.println("Oh yes! this is under dao ");
		JSONObject obj = new JSONObject();
		try
		{
			
			Database database = new Database();
			connection = database.Get_Connection();
			
			
			String query = "select o.*,c.id,c.discount,c.type as coupon_type from order_master o FULL JOIN coupon_master c ON o.coupon_id =cast(c.id as character varying) where o.id ="+uid;
        	// String query = "select * from order_master where id= '" +uid+"'" ;
        	
        	
        	
			PreparedStatement ps = connection.prepareStatement(query);
			
			
			
			
			
			ResultSet rs = ps.executeQuery();
			System.out.println("This is here also");
			
		        while (rs.next()) {
		            
		        	System.out.println(rs.getDouble("cart_discount"));
		        	System.out.println(rs.getString("coupon_discount"));
		        	
		         //   JSONObject obj = new JSONObject();
		            obj.put("net_cart_amount", rs.getString("cart_amount"));
		            obj.put("cart_discount_amount", rs.getDouble("cart_discount"));
		            String c_dis_amt = rs.getString("discount");
		            obj.put("coupon_discount_amount",c_dis_amt);
		            obj.put("coupon_type",rs.getString("coupon_type"));
		            
		           
		        }
		        
		
		}
		catch(Exception e)
		{
			throw e;
		}
		return obj;
	}
	
}
