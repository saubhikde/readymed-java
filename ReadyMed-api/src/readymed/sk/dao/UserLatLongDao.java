package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


import readymed.sk.dto.UserLatLongBean;
import readymed.sk.utility.Database;

public class UserLatLongDao {
	public ArrayList<UserLatLongBean> GetUserLatLong(String uid) throws Exception
	{
		
		Connection dbConn = null;
		System.out.println("fetching lat long");
		ArrayList<UserLatLongBean> feedData = new ArrayList<UserLatLongBean>();
		try
		{
			
				Database database = new Database();
				dbConn = database.Get_Connection();
			

			int sid = Integer.parseInt(uid);
			PreparedStatement ps = dbConn.prepareStatement("SELECT * FROM user_master where id= "+ sid);
		
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				UserLatLongBean feedObject = new UserLatLongBean();
				
				feedObject.setU_lat(rs.getDouble("u_lat"));
				feedObject.setU_lon(rs.getDouble("u_long"));
				feedObject.setUid(rs.getString("id"));
				
				
				
				feedData.add(feedObject);
			}
			return feedData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
}
