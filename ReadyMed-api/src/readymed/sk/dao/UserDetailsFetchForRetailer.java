package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;


import readymed.sk.utility.Database;

public class UserDetailsFetchForRetailer {

	
	public JSONArray GetFeeds(String User_ID) throws Exception
	{
		Connection dbConn = null;
		System.out.println("Oh yes! this is under dao "+User_ID+" ** ");
		JSONArray jsonArray = new JSONArray();
		try
		{
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int userid = Integer.parseInt(User_ID);
			PreparedStatement ps = dbConn.prepareStatement("SELECT * FROM user_master WHERE id=?");
			ps.setInt(1,userid);
			
			ResultSet rs = ps.executeQuery();
			 while (rs.next()) {
		            int total_rows = rs.getMetaData().getColumnCount();
		            JSONObject obj = new JSONObject();
		            for (int i = 0; i < total_rows; i++) {
		                obj.put(rs.getMetaData().getColumnLabel(i + 1)
		                        .toLowerCase(), rs.getObject(i + 1));
		            }
		            jsonArray.put(obj);
		        }
		        
			return jsonArray;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	
	
	
}
