package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import readymed.sk.utility.Database;

public class UserUpdateDao {
			
	
	public static boolean updateUser(String uid,String uname,String email,String address,String city,String pin) throws Exception{
		
		boolean updateStatus = false;
		Connection dbConn = null;
		System.out.println("In update Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int userid = Integer.parseInt(uid);
			
			
			PreparedStatement ps = dbConn.prepareStatement("update user_master set user_name=?, email_id =?,address=?,city=?,pin=? where id = ?");
			ps.setString(1, uname);
			ps.setString(2,email);
			ps.setString(3, address);
			ps.setString(4, city);
			ps.setString(5, pin);
			
			ps.setInt(6, userid);
			
			int record = ps.executeUpdate();
			if (record > 0) {
				updateStatus = true;
			}
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return updateStatus;
	}
}
