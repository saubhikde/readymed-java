package readymed.sk.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import readymed.sk.utility.DBConnection;
import readymed.sk.utility.Database;





public class RegisterDao {
	public static boolean insertUser(String uname,String phone,String email,String address,String city,String pin,Double lat_d,Double long_d) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		System.out.println("In Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String query = "INSERT into user_master(user_name,mobile_no, email_id,address,city,pin,u_lat,u_long) values('"+uname+ "',"+"'"
					+ phone + "','" + email + "','" +address+ "','"+city+"','"+pin+"','"+lat_d+"','"+long_d+ "')";
			System.out.println(query);
			Statement stmt = dbConn.createStatement();
			int records = stmt.executeUpdate(query);
			//System.out.println(records);
			//When record is successfully inserted
			if (records > 0) {
				insertStatus = true;
			}
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return insertStatus;
	}
	
	public static String getUserId(String uname,String phone) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		String id = null;
		System.out.println("In a Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("SELECT id FROM user_master WHERE user_name=? and mobile_no = ? ");
			ps.setString(1,uname);
			ps.setString(2, phone);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			int	idInt= rs.getInt("id");
			
			id= String.valueOf(idInt);
				/*ScanBeans feedObject = new ScanBeans();
				feedObject.setPatient_mob(rs.getString("patient_mob"));
				feedObject.setPatient_name(rs.getString("patient_name"));
				feedObject.setMsg_id(rs.getString("msg_id"));
				feedObject.setMessage(rs.getString("message"));
				feedObject.setUser_id(rs.getString("user_id_fk"));
				feedData.add(feedObject);*/
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return id;
	}
	
	public static boolean updateUser(String uname,String phone,String email,String add,String city,String pin,Double lat,Double lon) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		String id = null;
		System.out.println("In u Register Dao");
		try {
			try {
				Database database = new Database();
				dbConn = database.Get_Connection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PreparedStatement ps = dbConn.prepareStatement("update user_master set user_name=?, email_id =?,address=?,city=?,pin=?,u_lat=?,u_long=? where mobile_no=?");
			ps.setString(1, uname);
			ps.setString(2,email);
			ps.setString(3, add);
			ps.setString(4, city);
			ps.setString(5, pin);
			ps.setDouble(6, lat);
			ps.setDouble(7, lon);
			
			ps.setString(8, phone);
			
			int record = ps.executeUpdate();
			if (record > 0) {
				insertStatus = true;
			}
			
		} catch (SQLException sqle) {
			//sqle.printStackTrace();
			throw sqle;
		} catch (Exception e) {
			//e.printStackTrace();
			// TODO Auto-generated catch block
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return insertStatus;
	}
	

}

