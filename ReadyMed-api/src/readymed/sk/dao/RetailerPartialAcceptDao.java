package readymed.sk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.print.attribute.Size2DSyntax;

import readymed.sk.dto.RetailerAcceptBean;
import readymed.sk.utility.Database;

public class RetailerPartialAcceptDao {
			public int doPartialAccept(ArrayList<RetailerAcceptBean> acceptlist,ArrayList<RetailerAcceptBean> rejectlist,int shop_id,int accept_shop_id) throws Exception{
				boolean partialstatus = false;
				int key = 0;
				try{
					
				
				
				 key = insertAssignMaster(rejectlist,shop_id);
				
				 partialstatus = insertLineAssignMaster(key,acceptlist,rejectlist,shop_id);
			
				insertPartialAcceptMaster(acceptlist, accept_shop_id);
				
				doUpdatePartialacceptStatus(acceptlist);
				
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				return key;
				
			}
			
			public int insertAssignMaster(ArrayList<RetailerAcceptBean> rejectlist,int shop_id) throws Exception{
				boolean insertStatus = false;
				
				int key = 0;
				
				Connection dbConn = null;
				System.out.println("In Register Dao");
				try {
					
						Database database = new Database();
						dbConn = database.Get_Connection();
					
				int orderid = Integer.parseInt(rejectlist.get(0).getOrderid());
				int uid = Integer.parseInt(rejectlist.get(0).getUserid());
				int sid = shop_id;
				String asgn_type = "partial";
				String assign_by = "shop";
				
						
						
					
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					
					String dateInString = rejectlist.get(0).getDate_time();
					 
					
				 
						Date utildate = formatter.parse(dateInString);
						System.out.println(utildate);
						System.out.println(formatter.format(utildate));
				 
					
					java.sql.Timestamp sqlDate = new java.sql.Timestamp(utildate.getTime());
					System.out.println("-->"+sqlDate);
					
					
					String query = "INSERT into assign_master(order_id,shop_id,user_id,assign_type,assigned_by,assign_date) values('"+orderid+ "',"+"'"
							+ sid + "','" + uid + "','" +asgn_type+ "','"+assign_by+ "','"+sqlDate+ "')";
					System.out.println(query);
					Statement stmt = dbConn.createStatement();
					int records = stmt.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
					//System.out.println(records);
					//When record is successfully inserted
					
					ResultSet rs = stmt.getGeneratedKeys();
					if ( rs.next() ) {
					    // Retrieve the auto generated key(s).
					     key = rs.getInt(1);
					}
					
					if (records > 0) {
						insertStatus = true;
					}
					
				} catch (Exception e) {
					//e.printStackTrace();
					// TODO Auto-generated catch block
					if (dbConn != null) {
						dbConn.close();
					}
					throw e;
				}  finally {
					if (dbConn != null) {
						dbConn.close();
					//	dbConn.commit();
					}
				}
				
				
				return key;
			}
			
			
			public boolean insertLineAssignMaster(int key,ArrayList<RetailerAcceptBean> acceptlists,ArrayList<RetailerAcceptBean> rejectlists,int sid){
				boolean insertLineStatus = false;
				
				Connection dbConn = null;
				System.out.println("In Register Dao");
				try {
					try {
						Database database = new Database();
						dbConn = database.Get_Connection();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				for(int i = 0;i<rejectlists.size();i++){
					
					int assign_id = key;
					int oid = Integer.parseInt(rejectlists.get(i).getOrderid());
				    int prod_id = Integer.parseInt(rejectlists.get(i).getProduct_id());
					
					
				    String query = "INSERT into line_assign_master(assign_id,order_id,shop_id,product_id) values('"+assign_id+ "',"+"'"
							+ oid + "','" + sid + "','" +prod_id+  "')";
					System.out.println(query);
					Statement stmt = dbConn.createStatement();
					int records = stmt.executeUpdate(query);
					//System.out.println(records);
					//When record is successfully inserted
					if (records > 0) {
						insertLineStatus = true;
					}
				    
				    
					
				}
				
			
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				
				
				
				return insertLineStatus;
			}
			
			public boolean insertPartialAcceptMaster(ArrayList<RetailerAcceptBean> acceptlists,int accpt_shop_id){
				boolean insertLineStatus = false;
				
				Connection dbConn = null;
				System.out.println("In partial accept master Dao");
				try {
					try {
						Database database = new Database();
						dbConn = database.Get_Connection();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				for(int i = 0;i<acceptlists.size();i++){
					
					
					int oid = Integer.parseInt(acceptlists.get(i).getOrderid());
					int sid = accpt_shop_id;
				    int prod_id = Integer.parseInt(acceptlists.get(i).getProduct_id());
					
					
				    String query = "INSERT into partial_accept_master(order_id,shop_id,product_id) values('"+oid+ "',"+"'"
							+ sid + "','" + prod_id +  "')";
					System.out.println(query);
					Statement stmt = dbConn.createStatement();
					int records = stmt.executeUpdate(query);
					//System.out.println(records);
					//When record is successfully inserted
					if (records > 0) {
						insertLineStatus = true;
					}
				    
				    
					
				}
				
			
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				
				
				
				return insertLineStatus;
			}
			
			
			public static boolean doUpdatePartialacceptStatus(ArrayList<RetailerAcceptBean> accptlist) throws SQLException, Exception {
				boolean updateStatus = false;
				Connection dbConn = null;
				String id = null;
				System.out.println("In u Register Dao");
				try {
					try {
						Database database = new Database();
						dbConn = database.Get_Connection();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					PreparedStatement ps = dbConn.prepareStatement("update assign_master set current_status= 'partial accepted' where shop_id=? and order_id = ?");
				
					ps.setInt(1,Integer.parseInt(accptlist.get(0).getShop_id()));
					ps.setInt(2, Integer.parseInt(accptlist.get(0).getOrderid()));
					
					
					
					int record = ps.executeUpdate();
					if (record > 0) {
						updateStatus = true;
					}
					
				} catch (SQLException sqle) {
					//sqle.printStackTrace();
					throw sqle;
				} catch (Exception e) {
					//e.printStackTrace();
					// TODO Auto-generated catch block
					if (dbConn != null) {
						dbConn.close();
					}
					throw e;
				} finally {
					if (dbConn != null) {
						dbConn.close();
					}
				}
				return updateStatus;
			}
			
			
			
			
}
